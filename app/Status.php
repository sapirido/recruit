<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Status extends Model
{
    public function candidateStatus()
    {
        return $this->hasMany('App\Candidate');
    }

    public static function next($status_id){
        $nextstages = DB::table('nextstages')->where('from',$status_id)->pluck('to');
        return self::find($nextstages)->all();
    }

    public static function allowed($from,$to){
        switch($from){
            case 1:
                if($to == 2 || $to == 3 ){
                    return true;
                }else{
                    return false;
                }
            case 3:
                if($to == 4 || $to == 5){
                    return true;
                }else{
                    return false;
                }
            default:
            return false;
        }
    }

}



