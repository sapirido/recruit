<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use Illuminate\Notifications\Notifiable;


class Department extends Model
{
    public static function getAllDepartments(){
        $allDepartments = DB::table('department')->get();
        return $allDepartments;
    }

    public function users(){
        return $this->hasMany('App\Users');
    }
}

