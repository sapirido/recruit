@extends('layouts.app')
@section('title','Candidates')
@section('content')
@if(Session::has('notallowed'))
<div class = 'alert alert-danger'>
    {{Session::get('notallowed')}}
</div>
@endif
                    <div class="d-flex justify-content-between" style="padding-bottom:20px;">
                        <h1> List of candidates</h1>
                        <a href="{{url('/candidates/create')}}" class="btn btn-primary btn-lg active" role="button" aria-pressed="true">Add new candidate</a>
                    </div>
                    <table class="table">
                        <tr>
                            <th>id</th><th>Name</th><th>Email</th><th>Owner</th><th>Status</th><th>Created</th><th>Updated</th><th>Update</th><th>Delete</th>    
                        </tr>
                        <!-- the table data --> 
                        @foreach($candidates as $candidate)
                            <tr>
                                <td>{{ $candidate->id}}</td>
                                <td>{{ $candidate->name}}</td>
                                <td>{{ $candidate->email}}</td>
                                <td>
                                <div class="dropdown">
                                    <button class="btn btn-secondary dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                        @if(isset($candidate->user_id))
                                            {{ $candidate->owner->name }}
                                        @else()
                                            Assign owner
                                        @endif
                                    </button>
                                    <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                                    @foreach($users as $user)
                                    <a class="dropdown-item" href="{{route('candidate.changeuser',[$candidate->id,$user->id])}}">{{$user->name}}</a>
                                    @endforeach
                                    </div>
                                </div>
                                </td>
                                <td>
                                
                                <div class="dropdown">
                                    @if (null!= App\Status::next($candidate->status_id))
                                    <button class="btn btn-secondary dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                        @if(isset($candidate->status_id))
                                            {{ $candidate->status->name }}
                                        @else
                                            Define status
                                        @endif
                                    </button>
                                    @else{{$candidate->status->name}}
                                    @endif

                                    @if(App\Status::next($candidate->status_id) != null )
                                    <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                                        @foreach(App\Status::next($candidate->status_id) as $status )
                                        <a class="dropdown-item" href="{{route('candidate.changestatus',[$candidate->id,$status->id])}}">{{$status->name}}</a>
                                        @endforeach
                                    </div>
                                    @endif
                                </div>
                                </td>
                                <td>{{ $candidate->created_at}}</td>
                                <td>{{ $candidate->updated_at}}<!-- <td><a href = "{{url('/candidates/create')}}">Edit</a></td> -->
                                <td><a href = "{{action('CandidatesController@edit',$row ?? $candidate->id)}}">Edit</a></td>
                               <!-- <td><a href = "{{action('CandidatesController@destroy',$row ?? $candidate->id)}}">Delete</a></td>-->
                                <td><a href = "{{route('candidate.delete',$candidate->id)}}">Delete</a></td>
                            </tr>
                        
                        @endforeach


                    </table>       
@endsection
