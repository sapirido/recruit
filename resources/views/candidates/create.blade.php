@extends('layouts.app')
@section('title','Create candidate')
@section('content')
            <div class="d-flex justify-content-center">
                 <div class="d-flex flex-column">
                    <h1> Create candidate</h1>
                    <form method = "post" action = "{{action('CandidatesController@store')}}">
                    @csrf
                    <div class="d-flex justify-content-center">
                        <label for = "name"> Candidate name </lable>
                        <input class="form-control" type = "text" name = "name">
                    </div>   
                    
                    <div class="d-flex justify-content-center">
                        <label for = "email"> Candidate email</lable>
                        <input class="form-control" type = "text" name = "email">
                    </div>

                    <div class="d-flex justify-content-center" style="padding-top:12px;">
                    <button htmlFor="submit" class="btn btn-primary">Create Candidate</button>
                    </div>

                    </form>
                </div>
            </div>
@endsection