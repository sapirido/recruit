@extends('layouts.app')
@section('title','Edit candidate')
@section('content')
            <div class="d-flex justify-content-center">
                 <div class="d-flex flex-column">
                    <h1> Edit candidate</h1>
                    @if(count($errors) > 0)
                        <div class="alert alert-danger">
                            <ul>
                            @foreach($errors->all() as $error)
                                <li>{{$error}}</li>
                            @endforeach
                            </ul>
                    @endif
                    <form method = "post" action = "{{action('CandidatesController@update',$id)}}">
                    @csrf
                    <input type="hidden" name="_method" value="PATCH" /> 
                    <div class="d-flex justify-content-center">
                        <label for = "name"> Candidate name </lable>
                        <input class="form-control" type = "text" name = "name" value = "{{$candidate->name}}" placeholder="Enter an updated name" >
                    </div>   
                    
                    <div class="d-flex justify-content-center">
                        <label for = "email"> Candidate email</lable>
                        <input class="form-control" type = "email" name = "email" value = "{{$candidate->email}}" placeholder="Enter an updated email" >
                    </div>
                    <div class="d-flex justify-content-center" style="padding-top:12px;">
                    <button htmlFor="submit" class="btn btn-primary">Update Candidate</button>
                    <!-- <input type ="submit" name = "submit" class="btn btn-primary" value = "Update Candidate"> -->
                    </div>

                    </form>
                 </div>
            </div>
@endsection
